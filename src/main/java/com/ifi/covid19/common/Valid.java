package com.ifi.covid19.common;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author binhnt22@viettel.com.vn
 * @since May 2012
 * @version 1.1
 */
public final class Valid {

    public static String prefixCode = "856";
    public static String prefixIsdnMb = "20";
    public static String prefixIsdnHp = "30";
    private static final String PATTERN_IMAGE = "([^\\\\s]+(.*?)\\\\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$)";

    /**
     * alphabeUpCaseNumber.
     */
    private static String alphabeUpCaseNumber = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static String mask = "0123456789_aAÃ¡Ã?Ã Ã€áº£áº¢áº¡áº Ã£ÃƒÃ¢Ã‚áº¥áº¤áº§áº¦áº©áº¨áº­áº¬áº«áºªÄƒÄ‚áº¯áº®áº±áº°áº³áº²áº·áº¶áºµáº´bBcCdDÄ‘Ä?eEÃ©Ã‰Ã¨Ãˆáº»áººáº¹áº¸áº½áº¼ÃªÃŠáº¿áº¾á»?á»€á»ƒá»‚á»‡á»†á»…á»„fFgGhHiIÃ­Ã?Ã¬ÃŒá»‰á»ˆá»‹IHÄ©Ä¨jJkKlLmMnNoOÃ³Ã“Ã²Ã’á»?á»Žá»?á»ŒÃµÃ•Ã´Ã”á»‘á»?á»“á»’á»•á»”á»™á»˜á»—á»–Æ¡Æ á»›á»šá»?á»œá»Ÿá»žá»£á»¢á»¡á» pPqQrRsStTuUÃºÃšÃ¹Ã™á»§á»¦á»¥á»¤Å©Å¨Æ°Æ¯á»©á»¨á»«á»ªá»­á»¬á»±á»°á»¯á»®vVwWxXyYÃ½Ã?á»³á»²á»·á»¶á»µá»´á»¹á»¸zZ";
    private static String maskEN = "0123456789_aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ";

    /**
     * ZERO.
     * @param len
     */
    public static String OTP(int len) {
        System.out.println("Generating OTP using random() : ");
        System.out.print("You OTP is : ");

        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new Random();

        char[] otp = new char[len];

        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otp);
    }

    public static String getStringBetween(String content, String start, String end) {
        try {
            int idxStart = content.indexOf(start);
            int idxEnd = content.lastIndexOf(end);
            return content.substring(idxStart + start.length(), idxEnd);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isEmptyStringArray(String[] array, boolean isBeautyIsdn) {
        int count = 0;
        for (String element : array) {
            if (element != null && !element.equals("")) {
                count++;
            }
        }
        if (isBeautyIsdn) {
            return count < 5;
        } else {
            return count < 3;

        }
    }
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX
            = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static Environment env;
    private static final String ZERO = "0";
    private static final String c[] = {"<", ">"};
    private static final String expansion[] = {"&lt;", "&gt;"};

    public static String changeDateToString(Date date) {
        return date.getDate() + "/" + (date.getMonth() + 1) + "/" + (date.getYear() + 1900);
    }

    public static String setMSISDN(String subNumber) {
        String result = "";
        if (subNumber == null) {
            return result;
        }
        String subNo = subNumber.trim();
        if (subNo.substring(0, 1).equals("0")) {
            result = subNo.substring(1);
        } else {
            result = subNo;
        }
        if (!result.startsWith("856")) {
            result = "856" + result;
        }
        return result;
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr.trim());
        return matcher.find();
    }

    public static boolean validateImageFile(MultipartFile file) {
        String fileType = file.getContentType();
        String type = fileType.split("/")[0];
        if (type.equals("images")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isFileNull(MultipartFile file) {
        return file.isEmpty();
    }

    public static String isdnCut(String isdn) {
        if (isdn != null) {
            isdn = isdn.replaceAll("\\s+", "");
            if (isdn.contains("+856")) {
                isdn = isdn.replace("+856", "");
            }
            if (isdn.startsWith("0")) {
                isdn = isdn.substring(1);
            }
        }

        return isdn;
    }

    public static File ConvertToImage(String image, String name) {
        try {
            BufferedImage img = null;
            byte[] imageByte = Base64.getDecoder().decode(image);
            String path = env.getProperty("file.upload-dir");
            OutputStream stream = new FileOutputStream(path + name + ".bmp");
            stream.write(imageByte);

            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            img = ImageIO.read(bis);
            bis.close();
            // write the image to a file
            File outputfile = new File(name + ".bmp");
            ImageIO.write(img, "bmp", outputfile);
            return outputfile;
        } catch (IOException ex) {
            return null;
        }
    }

    public static String fakeIdEsim(Long esimId) {
        String id = esimId.toString();
        Integer hi = Integer.valueOf(id);
        String regex = "%1$08d";
        String result = String.format(regex, hi);
        return result;
    }

    public static boolean isConvertToImage(String base64) {
        try {
            byte[] imageByte = Base64.getDecoder().decode(base64);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            BufferedImage img = ImageIO.read(bis);
            bis.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    //get size of base64 img
    public static double getFileSizeMegaBytes(File file) {
        return (double) file.length() / (15.3 * 1024 * 1024);
    }

    public static float getBase64FileSizeMegaBytes(String img) {
        try {
            byte[] imageByte = Base64.getDecoder().decode(img);

            return (float) (imageByte.length) / (1024 * 1024);
        } catch (Exception ex) {
            return 0;
        }

    }
//    public static MultipartFile bufferedImgToFile(BufferedImage bf) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        try {
//            ImageIO.write(bf, "jpg", baos);
//            baos.flush();
//            
//        } catch (IOException ex) {
//            Logger.getLogger(StringUtils.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        
//
//        
//    }

    public static File ByteToFile(byte[] bytes) {
        File outputFile = new File("output");
        try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
            outputStream.write(bytes);

            return outputFile;
        } catch (Exception e) {
            return null;
        }

    }

    public static byte[] Base64toByte(String s, String name) {
        // Note preferred way of declaring an array variable
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(s);
            OutputStream stream = new FileOutputStream("src/main/java/com/viettel/base/esim/image/" + name + ".bmp");
            stream.write(decodedBytes);

            return decodedBytes;
        } catch (Exception ex) {
            return null;
        }
    }

    public static Date validateJavaDate(String strDate) throws Exception{
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
            return simpleDateFormat.parse(strDate.trim());
    }

    public static boolean validateDateCurrent(Date date) {
        try {
            if (date.before(new Date())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }

    }

    public static boolean compareString(String str1, String str2) {
        String str1Temp = str1;
        String str2Temp = str2;
        if (str1Temp == null) {
            str1Temp = "";
        }
        if (str2Temp == null) {
            str2Temp = "";
        }

        if (str1Temp.equals(str2Temp)) {
            return true;
        }
        return false;
    }

    public static String convertFromLongToString(Long lng) throws Exception {
        return Long.toString(lng);
    }

    public static String[] convertFromLongToString(Long[] arrLong) throws Exception {
        String[] arrResult = new String[arrLong.length];
        for (int i = 0; i < arrLong.length; i++) {
            arrResult[i] = convertFromLongToString(arrLong[i]);
        }
        return arrResult;
    }

    public static long[] convertFromStringToLong(String[] arrStr) throws Exception {
        long[] arrResult = new long[arrStr.length];
        for (int i = 0; i < arrStr.length; i++) {
            arrResult[i] = Long.parseLong(arrStr[i]);
        }
        return arrResult;
    }

    public static long convertFromStringToLong(String value) throws Exception {
        return Long.parseLong(value);
    }


    /*
     * Check String that containt only AlphabeUpCase and Number Return True if
     * String was valid, false if String was not valid
     */
    public static boolean checkAlphabeUpCaseNumber(String value) {
        boolean result = true;
        for (int i = 0; i < value.length(); i++) {
            String temp = value.substring(i, i + 1);
            if (alphabeUpCaseNumber.indexOf(temp) == -1) {
                result = false;
                return result;
            }
        }
        return result;
    }

    public static boolean maskVN(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (mask.indexOf(str.charAt(i)) < 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean maskEN(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (maskEN.indexOf(str.charAt(i)) < 0) {
                return false;
            }
        }
        if (str.toLowerCase().charAt(0) < 'a' || str.toLowerCase().charAt(0) > 'z') {
            return false;
        }
        return true;
    }

    public static boolean isInteger(String str) {
        if (str == null || !str.trim().matches("[0-9]+$")) {
            return false;
        }
        return true;
    }

    public static String formatString(String str) {
        return " '" + str.trim().toLowerCase() + "'";
    }

    public static String formatLike(String str) {
        return "%" + str.trim().toLowerCase().replaceAll("_", "\\\\_") + "%";
    }

    public static String formatOrder(String str, String direction) {
        return " NLSSORT(" + str + ",'NLS_SORT=vietnamese') " + direction;
    }

    public static String formatFunction(String function, String str) {
        return " " + function + "(" + str + ") ";
    }

    public static String formatConstant(String str) {
        String str1 = "";
        int index = 0;
        String alphabe = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for (int i = 1; i < str.length(); i++) {
            if (alphabe.indexOf(str.charAt(i)) > 0) {
                str1 = str1 + str.substring(index, i).toUpperCase() + "_";
                index = i;
            }
        }
        str1 = str1 + str.substring(index, str.length()).toUpperCase() + "_";
        return str1;
    }

    public static void main(String[] args) {
        System.out.println(formatConstant("raHivePartition"));
    }

    public static boolean isLong(String str) {
        try {
            Long.valueOf(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean containSpecialCharacteristic(String str) {
        if (str == null) {
            return false;
        }
        List lstSpecialCharacteristic = new ArrayList<String>();
        lstSpecialCharacteristic.add("!");
        lstSpecialCharacteristic.add("@");
        lstSpecialCharacteristic.add("#");
        lstSpecialCharacteristic.add("%");
        lstSpecialCharacteristic.add("^");
        lstSpecialCharacteristic.add("&");
        lstSpecialCharacteristic.add("*");
        lstSpecialCharacteristic.add("(");
        lstSpecialCharacteristic.add(")");
        lstSpecialCharacteristic.add(" ");
        for (int i = 0; i < lstSpecialCharacteristic.size(); i++) {
            if (str.contains(lstSpecialCharacteristic.get(i).toString())) {
                return true;
            }
        }
        return false;
    }

    public static String upperFirstChar(String input) {
        if (isNullOrEmpty(input)) {
            return input;
        }
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    public static boolean isNullOrEmpty(String obj1) {
        return (obj1 == null || "".equals(obj1.trim()));
    }

    public static boolean isLongNullOrEmpty(Long obj1) {
        return (obj1 == null);
    }

    public static boolean isDoubleNullOrEmpty(Double obj1) {
        return (obj1 == null || "0D".equals(obj1));
    }

    //
    public static boolean isStringNullOrEmpty(Object obj1) {
        return obj1 == null || obj1.toString().trim().equals("");
    }
}