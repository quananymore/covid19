package com.ifi.covid19.common;

import com.ifi.covid19.model.District;
import com.ifi.covid19.model.Province;
import com.ifi.covid19.model.Ward;
import com.ifi.covid19.repos.DistrictRepo;
import com.ifi.covid19.repos.ProvinceRepo;
import com.ifi.covid19.repos.WardRepository;
import com.ifi.covid19.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

@Component
public class Utils {
    @Autowired
    private AddressService addressService;

    public String getProvinceName(Long id){
        Province province = addressService.findProvinceById(id);
//        return province.getName();
        return null;
    }

    public  String getDistrictName(Long id){
        District province = addressService.findDistrictById(id);
        return province.getName();
    }

    public  String getWardName(Long id){
        Ward province = addressService.findWardById(id);
        return province.getName();
    }

    public static String getCurrentStatus(Long status) {
        String strStatus = "";
        if (status == 0L) {
            strStatus = Constant.JUST_DETECTED;
        } else if (status == 1L) {
            strStatus = Constant.TREATING;
        } else if (status == 2L) {
            strStatus = Constant.DIED;
        } else if (status == 3L) {
            strStatus = Constant.CURED;
        }
        return strStatus;
    }
}
