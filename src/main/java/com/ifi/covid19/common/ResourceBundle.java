package com.ifi.covid19.common;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
public class ResourceBundle {

  MessageSourceAccessor messageSourceAccessor;
  private static final Logger logger = LogManager.getLogger(ResourceBundle.class);

  String locate;
  

//  public ResourceBundle(String locate) {
      public ResourceBundle() {
    List<Locale> LOCALES = Arrays.asList(
            new Locale("en")
//            new Locale("la"),
//            new Locale("vi"),
//            new Locale("cn")
            );
    try {
//      if (StringUtils.isStringNullOrEmpty(locate)) {
        this.messageSourceAccessor = new MessageSourceAccessor(messageSource(), Locale.lookup(Locale.LanguageRange.parse("en"), LOCALES));
//      } else {
//        this.messageSourceAccessor = new MessageSourceAccessor(messageSource());
//      }
    } catch (Exception ex) {
      ex.printStackTrace();;
      logger.error(ex);
    }
  }

  public MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename("/Bundle/config");
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setUseCodeAsDefaultMessage(true);
    return messageSource;
  }

  public String getResourceMessage(String key) {

    try {
      return messageSourceAccessor.getMessage(key);
    } catch (Exception ex) {
      ex.printStackTrace();;
      logger.error(ex);
      return key;
    }
  }
}