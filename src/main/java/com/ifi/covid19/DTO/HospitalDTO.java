package com.ifi.covid19.DTO;

import com.ifi.covid19.model.Hospital;
import com.ifi.covid19.service.AddressService;

public class HospitalDTO {
    private Long id;
    private String name;
    private String address;
    private String phone;
    private String province;
    private String ward;
    private String district;

    public HospitalDTO() {
    }

    public HospitalDTO toDTO(AddressService a, Hospital h) {
        this.id = h.getId();
        this.name = h.getName();
        this.address = h.getAddress();
        this.phone = h.getPhone();
        this.province = a.findProvinceById(h.getProvinceId()).getName();
        this.district = a.findDistrictById(h.getDistrictId()).getName();
        this.ward = a.findWardById(h.getWardId()).getName();
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
