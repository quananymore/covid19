package com.ifi.covid19.DTO;

import com.ifi.covid19.model.Contact;
import com.ifi.covid19.model.Person;
import com.ifi.covid19.service.AddressService;
import com.ifi.covid19.serviceImpl.AddressServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import java.util.Date;
public class ContactDTO {
    private AddressServiceImpl addressService;

    private Long id;

    private Long patientId;

    private Date dateContact;

    private String personName;

    private String gender;

    private String phone;

    private Date birthDate;

    private String address;

    private String province;
    private String ward;
    private String district;

    public ContactDTO() {
    }

    public ContactDTO(Long id, Long patientId, Date dateContact, Date birthDate, String personName, String phone, String gender, String address) {
        this.id = id;
        this.patientId = patientId;
        this.dateContact = dateContact;
        this.personName = personName;
        this.gender = gender;
        this.phone = phone;
        this.birthDate = birthDate;
        this.address = address;
//        this.province = province;
//        this.ward = ward;
//        this.district = district;
    }
    public ContactDTO toDTO( AddressService addressService,Contact c){
        this.id = c.getId();
        this.patientId = c.getPatientId();
        this.dateContact = c.getDateContact();
        this.personName = c.getPersonName();
        this.gender = c.getGender();
        this.phone = c.getPhone();
        this.birthDate = c.getBirthDate();
        this.address = c.getAddress();
        this.province = addressService.findProvinceById(c.getProvinceId()).getName();
        this.district = addressService.findDistrictById(c.getDistrict()).getName();
        this.ward = addressService.findWardById(c.getWardId()).getName();
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Date getDateContact() {
        return dateContact;
    }

    public void setDateContact(Date dateContact) {
        this.dateContact = dateContact;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
