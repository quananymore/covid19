package com.ifi.covid19.DTO;

import com.ifi.covid19.model.Hospital;
import com.ifi.covid19.model.Patient;
import com.ifi.covid19.model.Schedule;

import java.util.List;

public class PatientDetailDTO {
    private PatientDTO patient;
    private HospitalDTO hospital;
    private List<ContactDTO> contacts;
    private List<ScheduleDTO> schedules;

    public PatientDetailDTO() {
    }

    public PatientDTO getPatient() {
        return patient;
    }

    public void setPatient(PatientDTO patient) {
        this.patient = patient;
    }

    public HospitalDTO getHospital() {
        return hospital;
    }

    public void setHospital(HospitalDTO hospital) {
        this.hospital = hospital;
    }

    public List<ContactDTO> getContacts() {
        return contacts;
    }

    public void setContacts(List<ContactDTO> contacts) {
        this.contacts = contacts;
    }

    public List<ScheduleDTO> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ScheduleDTO> schedules) {
        this.schedules = schedules;
    }
}
