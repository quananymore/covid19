package com.ifi.covid19.DTO;

import com.ifi.covid19.model.Patient;
import com.ifi.covid19.service.AddressService;

import javax.persistence.Column;
import java.util.Date;

public class PatientDTO {
    private Long id;

    private String address;

    private Date birthDate;

    private String gender;

    private String identify;

    private String name;

    private String phoneNumber;

    private Long status;

    private Date detectionDate;

    private String image;

    private String province;

    private String ward;

    private String district;

    private String hospitalName;

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public PatientDTO toDTO(AddressService a, Patient p) {
        this.id = p.getId();
        this.address = p.getAddress();
        this.birthDate = p.getBirthDate();
        this.gender = p.getGender();
        this.identify = p.getIdentify();
        this.name = p.getName();
        this.phoneNumber = p.getPhoneNumber();
        this.status = p.getStatus();
        this.detectionDate = p.getDetectionDate();
        this.image = p.getImage();
        this.hospitalName = a.findHosById(p.getHospitalId()).getName();
        this.province = a.findProvinceById(p.getProvinceId()).getName();
        this.district = a.findDistrictById(p.getDistrictId()).getName();
        this.ward = a.findWardById(p.getWardId()).getName();
        return  this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Date getDetectionDate() {
        return detectionDate;
    }

    public void setDetectionDate(Date detectionDate) {
        this.detectionDate = detectionDate;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
