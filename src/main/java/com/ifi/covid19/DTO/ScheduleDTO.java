package com.ifi.covid19.DTO;

import com.ifi.covid19.model.Schedule;
import com.ifi.covid19.service.AddressService;

import java.util.Date;

public class ScheduleDTO {
    private Long id;
    private String detailLocation;
    private Date fromDate;
    private Date toDate;
    private Long patientId;
    private String province;
    private String district;
    private String ward;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetailLocation() {
        return detailLocation;
    }

    public void setDetailLocation(String detailLocation) {
        this.detailLocation = detailLocation;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public ScheduleDTO toDTO(AddressService addressService, Schedule schedule) {
        this.id = schedule.getId();
        this.detailLocation = schedule.getDetailLocation();
        this.fromDate = schedule.getFromDate();
        this.toDate = schedule.getToDate();
        this.patientId = schedule.getPatientId();
        this.province = addressService.findProvinceById(schedule.getProvinceId()).getName();
        this.district = addressService.findDistrictById(schedule.getDistrictId()).getName();
        this.ward = addressService.findWardById(schedule.getWardId()).getName();
        return this;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }
}
