package com.ifi.covid19.model;

import com.ifi.covid19.DTO.ContactDTO;
import com.ifi.covid19.common.Utils;
import com.ifi.covid19.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long patientId;

    private Date dateContact;

    private String personName;

    private String gender;

    private String phone;

    private Date birthDate;

    private String address;

    private Long provinceId;

    private Long district;

    private Long wardId;

    public Contact() {
    }

    public Contact(Long patientId, Date dateContact, Date birthDate, String personName, String phone, String gender, String address, Long provinceId, Long district, Long wardId) {
        this.patientId = patientId;
        this.dateContact = dateContact;
        this.personName = personName;
        this.gender = gender;
        this.phone = phone;
        this.birthDate = birthDate;
        this.address = address;
        this.provinceId = provinceId;
        this.district = district;
        this.wardId = wardId;
    }


    public ContactDTO toDTO(){
//        Utils utils = new Utils();

        try{
            ContactDTO dto = new ContactDTO(
                    id,
                    patientId,
                    dateContact,
                    birthDate,
                    personName,
                    phone,
                    gender,
                    address
            );
            return dto;
        }catch (Exception e){
            return null;
        }
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Date getDateContact() {
        return dateContact;
    }

    public void setDateContact(Date dateContact) {
        this.dateContact = dateContact;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getDistrict() {
        return district;
    }

    public void setDistrict(Long district) {
        this.district = district;
    }

    public Long getWardId() {
        return wardId;
    }

    public void setWardId(Long wardId) {
        this.wardId = wardId;
    }

}