package com.ifi.covid19.model;

import javax.persistence.*;

@Entity
@Table(name = "ward")
public class Ward {
    @Id
    private Long code;

    private String name;

    private String division_type;

    private String codename;

    private Long district_code;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision_type() {
        return division_type;
    }

    public void setDivision_type(String division_type) {
        this.division_type = division_type;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public Long getDistrict_code() {
        return district_code;
    }

    public void setDistrict_code(Long district_code) {
        this.district_code = district_code;
    }
}