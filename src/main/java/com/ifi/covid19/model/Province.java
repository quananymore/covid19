package com.ifi.covid19.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Entity
@Table(name = "province")
public class Province {
    @Id
    private Long code;
    @Column(name = "name")
    private String name;
    @Column(name = "division_type")
    private String division_type;
    @Column(name = "codename")
    private String codename;
    @Column(name="phone_code")
    private Long phone_code;

    public Province() {
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDivision_type() {
        return division_type;
    }

    public void setDivision_type(String division_type) {
        this.division_type = division_type;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public Long getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(Long phone_code) {
        this.phone_code = phone_code;
    }
}
