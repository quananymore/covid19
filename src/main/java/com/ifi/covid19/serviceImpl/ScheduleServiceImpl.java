package com.ifi.covid19.serviceImpl;

import com.ifi.covid19.model.Schedule;
import com.ifi.covid19.repos.PatientRepository;
import com.ifi.covid19.repos.ScheduleRepo;
import com.ifi.covid19.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleRepo scheduleRepo;

    @Override
    public List<Schedule> findByPatientId(Long id) {
        return scheduleRepo.findByPatientId(id);
    }

    @Override
    public Schedule save(Schedule schedule) {
        return scheduleRepo.save(schedule);
    }
}
