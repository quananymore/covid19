package com.ifi.covid19.serviceImpl;

import com.ifi.covid19.model.District;
import com.ifi.covid19.model.Hospital;
import com.ifi.covid19.model.Province;
import com.ifi.covid19.model.Ward;
import com.ifi.covid19.repos.DistrictRepo;
import com.ifi.covid19.repos.HospitalRepo;
import com.ifi.covid19.repos.ProvinceRepo;
import com.ifi.covid19.repos.WardRepository;
import com.ifi.covid19.service.AddressService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private WardRepository wardRepository;

    @Autowired
    private DistrictRepo districtRepo;

    @Autowired
    private ProvinceRepo provinceRepo;

    @Autowired
    private HospitalRepo hospitalRepo;

    @Override
    public void addProvince(List<Province> list) {
        provinceRepo.saveAll(list);
    }

    @Override
    public void addDistrict(List<District> list) {
        districtRepo.saveAll(list);
    }

    @Override
    public void addWard(Ward ward) {
            wardRepository.save(ward);
    }

    @Override
    public Ward findWardById(Long id) {
        try{
            return wardRepository.findByCode(id);

        }catch (Exception e){
        }
        return null;
    }

    @Override
    public District findDistrictById(Long id) {
        try{
            return districtRepo.findByCode(id);

        }catch (Exception e){
        }
        return null;
    }

    @Override
    public Province findProvinceById(Long id) {
        try{
            return provinceRepo.findByCode(id);

        }catch (Exception e){
        }
        return null;

    }

    public Hospital findHosById(Long id)  {
        try{
            return hospitalRepo.findByCode(id);

        }catch (Exception e){
        }
        return null;
    }

    @Override
    public List<Hospital> findAllHos() {
        return hospitalRepo.findAll();
    }

    @Override
    public List<Province> getProvinceAll() {
        return provinceRepo.findAll();
    }

    @Override
    public List<District> getDistrictByProvince(Long id) {
        return districtRepo.findByProvince(id);
    }

    @Override
    public List<Ward> getWardByDistrict(Long id) {
        return wardRepository.findByDistrict(id);
    }
}
