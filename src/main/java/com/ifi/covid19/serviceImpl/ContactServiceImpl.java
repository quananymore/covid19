package com.ifi.covid19.serviceImpl;

import com.ifi.covid19.model.Contact;
import com.ifi.covid19.model.Person;
import com.ifi.covid19.repos.ContactRepo;
import com.ifi.covid19.repos.PersonRepo;
import com.ifi.covid19.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ContactServiceImpl implements ContactService {
    @Autowired
    ContactRepo repo;

    @Autowired
    PersonRepo repoPerson;
    @Override
    public List<Contact> findByPatientId(Long id) {
        return repo.findByPatientId(id);
    }

    @Override
    public Contact save(Contact contact) {
        return repo.save(contact);
    }

    @Override
    public Person findPerson(Long personId) {
        return repoPerson.findByCode(personId);
    }
}
