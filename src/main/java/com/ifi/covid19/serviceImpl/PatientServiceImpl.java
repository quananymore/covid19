package com.ifi.covid19.serviceImpl;

import com.ifi.covid19.exception.PatientNotFoundException;
import com.ifi.covid19.model.Patient;
import com.ifi.covid19.repos.PatientRepository;
import com.ifi.covid19.service.PatientService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private static long STATUS_DELETED = 1L;
    private static long STATUS_JUST = 0L;
    private static long STATUS_TREATING = 1L;
    private static long STATUS_DIED = 2L;
    private static long STATUS_CURED = 3L;
    @Autowired
    private PatientRepository patientRepository;

    private static Logger log;
    @Override
    public Patient save(Patient patient) {
        return patientRepository.save(patient);
    }

    @Override
    public List<Patient> findAllActive() {
        return patientRepository.findAllActive();
    }

    @Override
    public Patient deletePatient(Long id) {
        Patient patient = patientRepository.findById(id).
                orElseThrow(() -> new PatientNotFoundException("Patient by id " + id + " was not found "));
        patient.setIsDelete(STATUS_DELETED);
        return patientRepository.save(patient);
    }

    @Override
    public List<Patient> searchWithStatusAndKeyWord(String kw,Long status){
        return patientRepository.findAllByStatusAndKeyword(status,kw);
    }

    @Override
    public List<Patient> searchWithKeyWord(String kw) {
        return patientRepository.findAllByKeyword(kw);
    }

    @Override
    public List<Patient> searchWithStatus(Long status){
        return patientRepository.findAllByStatus(status);
    }

    @Override
    public Patient findById(Long id){
        try {
            Patient patient = patientRepository.findById(id).
                    orElseThrow(() -> new PatientNotFoundException("Patient by id " + id + " was not found "));
            return patient;
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return null;
    }

}
