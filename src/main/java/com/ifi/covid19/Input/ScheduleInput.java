package com.ifi.covid19.Input;

import com.ifi.covid19.common.Valid;
import com.ifi.covid19.model.Schedule;

import java.util.Date;

public class ScheduleInput {
    private Long id;
    private Long provinceId;
    private Long districtId;
    private Long wardId;
    private String detailLocation;
    private String fromDate;
    private String toDate;
    private Long patientId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getWardId() {
        return wardId;
    }

    public void setWardId(Long wardId) {
        this.wardId = wardId;
    }

    public String getDetailLocation() {
        return detailLocation;
    }

    public void setDetailLocation(String detailLocation) {
        this.detailLocation = detailLocation;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public Schedule toModel(){
        try{
            Schedule schedule = new Schedule(
                    id,
                    provinceId,
                    districtId,
                    wardId,
                    detailLocation,
                    Valid.validateJavaDate(fromDate),
                    Valid.validateJavaDate(toDate),
                    patientId
            );
            return schedule;
        }catch (Exception e){
            return null;
        }
    }
}
