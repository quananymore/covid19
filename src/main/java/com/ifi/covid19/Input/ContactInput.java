package com.ifi.covid19.Input;

import com.ifi.covid19.common.Valid;
import com.ifi.covid19.model.Contact;

import java.util.Date;

public class ContactInput {

    private Long patientId;

    private String dateContact;

    private String birthDate;

    private String personName;

    private String phone;

    private String gender;

    private String address;

    private Long provinceId;

    private Long district;

    private Long wardId;

    public Contact toModel() {
        try {
            Contact model = new Contact(
                    patientId,
                    Valid.validateJavaDate(dateContact),
                    Valid.validateJavaDate(birthDate),
                    personName,
                    phone,
                    gender,
                    address,
                    provinceId,
                    district,
                    wardId
            );
            return model;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public ContactInput(Long patientId, String dateContact, String birthDate, String personName, String phone, String gender, String address, Long provinceId, Long district, Long wardId) {
        this.patientId = patientId;
        this.dateContact = dateContact;
        this.birthDate = birthDate;
        this.personName = personName;
        this.phone = phone;
        this.gender = gender;
        this.address = address;
        this.provinceId = provinceId;
        this.district = district;
        this.wardId = wardId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public ContactInput() {
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getDateContact() {
        return dateContact;
    }

    public void setDateContact(String dateContact) {
        this.dateContact = dateContact;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getDistrict() {
        return district;
    }

    public void setDistrict(Long district) {
        this.district = district;
    }

    public Long getWardId() {
        return wardId;
    }

    public void setWardId(Long wardId) {
        this.wardId = wardId;
    }


}
