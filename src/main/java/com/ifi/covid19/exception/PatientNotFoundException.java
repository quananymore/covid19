package com.ifi.covid19.exception;

public class PatientNotFoundException extends RuntimeException {
    public PatientNotFoundException(String msg) {
        super(msg);
    }
}
