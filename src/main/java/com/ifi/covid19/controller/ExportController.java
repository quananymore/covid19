//package com.ifi.covid19.controller;
//
//import com.ifi.covid19.DTO.PatientDetailDTO;
//import com.ifi.covid19.common.Constant;
//import com.ifi.covid19.common.Storage;
//import org.springframework.core.io.InputStreamResource;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.FileWriter;
//import java.io.IOException;
//
//public class ExportController {
//    @GetMapping("/patient/export/{id}")
//    public ResponseEntity<Object> downloadFile(@PathVariable("id") Long id) throws IOException {
//        PatientDetailDTO p = findById(id);
//        String fileName = new Storage().exportWord(p);
//        File file = new File(Constant.REPORT_PATH+fileName);
//        FileWriter filewriter =  null;
//        try {
//
//            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
//            HttpHeaders headers = new HttpHeaders();
//            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
//            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
//            headers.add("Pragma", "no-cache");
//            headers.add("Expires", "0");
//
//            ResponseEntity<Object> responseEntity = ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
//            return responseEntity;
//        } catch (Exception e ) {
//            return new ResponseEntity<>("error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
//        } finally {
//            if(filewriter!=null)
//                filewriter.close();
//        }
//    }
//}
/*Phan parse du lieu cac tinh thanh pho cua VietNam tu API vao db*/
//    @GetMapping("/ward/add")
//    public void addWard() {
//        List<Ward> wards = new ArrayList<>();
//        try {
//            JSONArray wardJson = getArrayObject("ward.json");
//            for (Object o : wardJson) {
//                JSONObject obj = (JSONObject) o;
//                Ward province = new Gson().fromJson(obj.toJSONString(), Ward.class);
//                addressService.addWard(province);
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//    @GetMapping("/province/add")
//    public void addProvince() {
//        List<Province> provinces = new ArrayList<>();
//        try {
//            JSONArray wardJson = getArrayObject("Province.json");
//            for (Object o : wardJson) {
//                JSONObject obj = (JSONObject) o;
//                Province province = new Gson().fromJson(obj.toJSONString(), Province.class);
//                provinces.add(province);
//
//            }
//            addressService.addProvince(provinces);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }
//    @GetMapping("/district/add")
//    public void addDistrict() {
//        List<District> wards = new ArrayList<>();
//        try {
//            JSONArray wardJson = getArrayObject("District.json");
//            for (Object o : wardJson) {
//                JSONObject obj = (JSONObject) o;
//                District province = new Gson().fromJson(obj.toJSONString(), District.class);
//                wards.add(province);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        addressService.addDistrict(wards);
//
//
//    }
//    private static String pathModel =System.getProperty("user.dir")+ "\\\\src\\\\main\\\\resources\\\\Json\\\\";
//
//    public static JSONArray getArrayObject(String fileName) {
//        if (fileName == null || "".equals(fileName)) {
//            return null;
//        }
//        String filePath = pathModel + fileName;
//        JSONParser jsonParser = new JSONParser();
//        JSONArray a = new JSONArray();
//        try (FileReader reader = new FileReader(filePath)) {
//            //Read JSON file
//
//            a = (JSONArray) jsonParser.parse(reader);
//
//            return a;
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return a;
//    }
