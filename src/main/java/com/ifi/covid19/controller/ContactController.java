//package com.ifi.covid19.controller;
//
//import com.ifi.covid19.DTO.*;
//import com.ifi.covid19.Input.ContactInput;
//import com.ifi.covid19.common.*;
//import com.ifi.covid19.model.*;
//import com.ifi.covid19.repos.PatientRepository;
//import com.ifi.covid19.service.AddressService;
//import com.ifi.covid19.service.ContactService;
//import com.ifi.covid19.service.PatientService;
//import com.ifi.covid19.service.ScheduleService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.*;
//import java.util.ResourceBundle;
//
//@CrossOrigin(origins = "http://localhost:3000")
//@RestController
//public class ContactController {
//    Logger log = LoggerFactory.getLogger(ContactController.class);
//
//    @Autowired
//    private ContactService contactService;
//    @Autowired
//    private AddressService addressService;
//
//    @PostMapping("/contact/add")
//    public Output addContact(@RequestBody ContactInput contact) {
//        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
//        Output out = new Output();
//        contactService.save(contact.toModel());
//        out.setMessage(r.getString("1"));
//        return out;
//    }
//
//    @GetMapping("/contact/{patient_id}")
//    public List<ContactDTO> getContactByPatient(@PathVariable("patient_id") Long id) {
//        List<Contact> schedules = contactService.findByPatientId(id);
//        List<ContactDTO> scheduleDTOS = new ArrayList<>();
//        for (Contact schedule : schedules) {
//            scheduleDTOS.add(convertDTOHos(schedule));
//        }
//        return scheduleDTOS;
//    }
//
//
//    public ContactDTO convertDTOHos(Contact patient) {
//        ContactDTO patientDTO = patient.toDTO();
//        patientDTO.setProvince(addressService.findProvinceById(patient.getProvinceId()).getName());
//        patientDTO.setWard(addressService.findWardById(patient.getWardId()).getName());
//        patientDTO.setDistrict(addressService.findDistrictById(patient.getDistrict()).getName());
//        return patientDTO;
//    }
//}
//
