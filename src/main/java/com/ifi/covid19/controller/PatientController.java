package com.ifi.covid19.controller;

import com.ifi.covid19.Covid19Application;
import com.ifi.covid19.DTO.*;
import com.ifi.covid19.Input.ContactInput;
import com.ifi.covid19.Input.Input;
import com.ifi.covid19.Input.ScheduleInput;
import com.ifi.covid19.common.Constant;
import com.ifi.covid19.common.Output;
import com.ifi.covid19.common.Storage;
import com.ifi.covid19.common.Valid;
import com.ifi.covid19.model.*;
import com.ifi.covid19.service.AddressService;
import com.ifi.covid19.service.ContactService;
import com.ifi.covid19.service.PatientService;
import com.ifi.covid19.service.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(value = {"*"}, exposedHeaders = {"Content-Disposition"})
@RestController
public class PatientController {
    Logger log = LoggerFactory.getLogger(PatientController.class);
    @Autowired
    private PatientService patientService;

    @Autowired
    private AddressService addressService;
    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private ContactService contactService;

    @GetMapping("/patients/export")
    public void exportExcel() {
        List<PatientDTO> patients = getAll();
//        return convertDTOs(patients);
        new Storage().exportExcel(patients);
    }

    @GetMapping("/patient/all")
    public List<PatientDTO> getAll() {
        List<Patient> patients = patientService.findAllActive();
        return convertDTOs(patients);
    }

    public static void test(){
        System.out.println("hello Ted");
    }

    @PostMapping("/patient/add")
    public Output addPatient(@RequestBody Input p) {
        Patient patient = new Patient();
        Output out = new Output();
        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
        if (Valid.isNullOrEmpty(p.getPhoneNumber())) {
            out.setResult(2);
            out.setMessage(r.getString("2"));
            return out;
        }
        String phone = p.getPhoneNumber().trim();
        if (!Valid.isInteger(phone)) {
            out.setResult(3);
            out.setMessage(r.getString("3"));
            return out;
        }
        if (Valid.isLongNullOrEmpty(p.getHospitalId())) {
            out.setResult(4);
            out.setMessage(r.getString("4"));
            return out;
        }
        Long hos = p.getHospitalId();
        if (Valid.isLongNullOrEmpty(p.getProvinceId())) {
            out.setResult(5);
            out.setMessage(r.getString("5"));
            return out;
        }
        Long province = p.getProvinceId();

        if (Valid.isLongNullOrEmpty(p.getDistrictId())) {
            out.setResult(6);
            out.setMessage(r.getString("6"));
            return out;
        }
        Long district = p.getDistrictId();

        if (Valid.isLongNullOrEmpty(p.getWardId())) {
            out.setResult(7);
            out.setMessage(r.getString("7"));
            return out;

        }
        Long ward = p.getWardId();

        if (Valid.isNullOrEmpty(p.getBirthDate())) {
            out.setResult(8);
            out.setMessage(r.getString("8"));
            return out;

        }
        String birthDate = p.getBirthDate();
        Date birth = null;
        try {
            birth = Valid.validateJavaDate(birthDate);
        } catch (Exception e) {
            log.error(e.getMessage());
            out.setResult(9);
            out.setMessage(r.getString("9"));
            return out;
        }
        if (Valid.isNullOrEmpty(p.getDetectionDate())) {
            out.setResult(10);
            out.setMessage(r.getString("10"));
            return out;
        }
        String detectDate = p.getDetectionDate();
        Date detect = null;
        try {
            detect = Valid.validateJavaDate(detectDate);
        } catch (Exception e) {
            log.error(e.getMessage());
            out.setResult(11);
            out.setMessage(r.getString("11"));
            return out;
        }
        if (Valid.isNullOrEmpty(p.getIdentify())) {
            out.setResult(12);
            out.setMessage(r.getString("12"));
            return out;
        }
        String identify = p.getIdentify().trim();
        if (!Valid.isInteger(identify)) {
            out.setResult(13);
            out.setMessage(r.getString("13"));
            return out;
        }
        if (Valid.isNullOrEmpty(p.getName())) {
            out.setResult(14);
            out.setMessage(r.getString("14"));
            return out;
        }
        String name = p.getName().trim();
        if (Valid.isNullOrEmpty(p.getGender())) {
            out.setResult(15);
            out.setMessage(r.getString("15"));
            return out;
        }
        String gender = p.getGender().trim();

        if (Valid.isNullOrEmpty(p.getAddress())) {
            out.setResult(16);
            out.setMessage(r.getString("16"));
            return out;
        }
        String address = p.getAddress().trim();
        if (Valid.isLongNullOrEmpty(p.getStatus())) {
            out.setResult(17);
            out.setMessage(r.getString("17"));
            return out;
        }
        Long status = p.getStatus();

        if (Valid.isNullOrEmpty(p.getImage())) {
            out.setResult(18);
            out.setMessage(r.getString("18"));
            return out;

        }
        String image = p.getImage().trim();

        Map<String, String> map = new HashMap<>();
        Storage storage = new Storage();
        Map<String, String> mapImg = storage.storeFile(image, identify);
        mapImg.entrySet().stream().map((entry) -> {
            String key = entry.getKey();
            String value = entry.getValue();
            map.put(key, value);
            return key;
        }).forEachOrdered((key) -> {
            patient.setImage(key);
        });
        patient.setAddress(address);
        patient.setIdentify(identify);
        patient.setGender(gender);
        patient.setPhoneNumber(phone);
        patient.setHospitalId(hos);
        patient.setBirthDate(birth);
        patient.setDetectionDate(detect);
        patient.setWardId(ward);
        patient.setDistrictId(district);
        patient.setProvinceId(province);
        patient.setStatus(status);
        patient.setIsDelete(Constant.NEW);
        patient.setName(name);
        patientService.save(patient);
        out.setResult(1);
        out.setMessage(r.getString("1"));

        return out;
    }

    @PostMapping("/patient/update")
    public Output updatePatient(@RequestBody Patient patient) {
        Output out = new Output();
        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
        if (Valid.isLongNullOrEmpty(patient.getId())) {
            out.setResult(19);
            out.setMessage(r.getString("19"));
            return out;
        }
        if (Valid.isLongNullOrEmpty(patient.getStatus())) {
            out.setResult(20);
            out.setMessage(r.getString("20"));
            return out;
        }
        Patient patientUpdate = patientService.findById(patient.getId());
        patientUpdate.setStatus(patient.getStatus());
        patientService.save(patientUpdate);
        out.setMessage(r.getString("1"));
        return out;
    }

    @DeleteMapping("/patient/delete/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable("id") Long id) {
        Patient deletePatient = patientService.deletePatient(id);
        return new ResponseEntity<>(deletePatient, HttpStatus.OK);
    }

    @GetMapping("/patient/{id}")
    public PatientDetailDTO findById(@PathVariable("id") Long id) {
        PatientDetailDTO detailDTO = new PatientDetailDTO();
        Patient patient = patientService.findById(id);
        String image = Storage.getImage(patient.getImage());
        patient.setImage(image);
        if (patient == null) {
            return null;
        } else {
            detailDTO.setPatient(new PatientDTO().toDTO(addressService,patient));
            Hospital hospital = addressService.findHosById(patient.getHospitalId());
            if (hospital != null) {
                detailDTO.setHospital(new HospitalDTO().toDTO(addressService,hospital));
            }
            detailDTO.setContacts(getContactByPatient(patient.getId()));
            detailDTO.setSchedules(getScheduleByPatient(patient.getId()));
        }
        return detailDTO;
    }

    @GetMapping("/export/{id}")
    public ResponseEntity<byte[]> downloadFile(@PathVariable("id") Long id) throws IOException {
        PatientDetailDTO p = findById(id);
        String fileName = new Storage().exportWord(p);
        File  file = new File(Constant.REPORT_PATH+fileName);
        HttpHeaders header = new HttpHeaders();
        Path path = file.toPath();
        String mimeType = Files.probeContentType(path);
        System.out.println(mimeType);
        header.setContentType(MediaType.valueOf(mimeType));
        byte[] data = Files.readAllBytes(path);
        header.setContentLength(data.length);
        header.set("Content-Disposition", "attachment; filename=" + fileName);
        return new ResponseEntity<>(data, header, HttpStatus.OK);
    }

    @RequestMapping("/patients")
    public List<PatientDTO> search(@Param("name") String name, @Param("status") String status) {
        List<Patient> patients;
        if (Valid.isNullOrEmpty(name) && Valid.isNullOrEmpty(status)) {
            patients = patientService.findAllActive();
        } else if (!Valid.isNullOrEmpty(name) && Valid.isNullOrEmpty(status)) {
            patients = patientService.searchWithKeyWord(name);
        } else if (Valid.isNullOrEmpty(name) && !Valid.isNullOrEmpty(status)) {
            patients = patientService.searchWithStatus(Long.valueOf(status.trim()));
        } else {
            patients = patientService.searchWithStatusAndKeyWord(name, Long.valueOf(status.trim()));
        }
        return convertDTOs(patients);
    }

    public List<PatientDTO> convertDTOs(List<Patient> patients){
        List<PatientDTO> patientDTOS = new ArrayList<>();
        if (patients.size() > 0) {
            for (Patient patient : patients) {
                patientDTOS.add(new PatientDTO().toDTO(addressService,patient));
            }
            return patientDTOS;
        } else {
            return null;
        }
    }
    @GetMapping("/hospital")
    public List<Hospital> getHosAll() {
        return addressService.findAllHos();
    }

    @GetMapping("/province")
    public List<Province> getProvinceAll() {
        return addressService.getProvinceAll();
    }

    @GetMapping("/district/{province_id}")
    public List<District> getDistrictByProvince(@PathVariable("province_id") Long id) {
        return addressService.getDistrictByProvince(id);
    }

    @GetMapping("/ward/{district_id}")
    public List<Ward> getWardByDistrict(@PathVariable("district_id") Long id) {
        return addressService.getWardByDistrict(id);
    }

    @PostMapping("/schedule/add")
    public Output addContact(@RequestBody ScheduleInput schedule) {
        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
        Output out = new Output();
        scheduleService.save(schedule.toModel());
        out.setMessage(r.getString("1"));
        return out;
    }


    @GetMapping("/schedule/{patient_id}")
    public List<ScheduleDTO> getScheduleByPatient(@PathVariable("patient_id") Long id) {
        List<Schedule> schedules = scheduleService.findByPatientId(id);
        List<ScheduleDTO> scheduleDTOS = new ArrayList<>();
        for (Schedule schedule : schedules) {
//            scheduleDTOS.add(convertDTOHos(schedule));
            scheduleDTOS.add(new ScheduleDTO().toDTO(addressService,schedule));
        }
        return scheduleDTOS;
    }

    @PostMapping("/contact/add")
    public Output addContact(@RequestBody ContactInput contact) {
        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
        Output out = new Output();
        contactService.save(contact.toModel());
        out.setMessage(r.getString("1"));
        return out;
    }

    @GetMapping("/contact/{patient_id}")
    public List<ContactDTO> getContactByPatient(@PathVariable("patient_id") Long id) {
        List<Contact> contacts = contactService.findByPatientId(id);
        List<ContactDTO> contactDTOS = new ArrayList<>();
        for (Contact contact : contacts) {
//            contactDTOS.add(convertDTOHos(contact));
            contactDTOS.add(new ContactDTO().toDTO(addressService,contact));
        }
        return contactDTOS;
    }

}


