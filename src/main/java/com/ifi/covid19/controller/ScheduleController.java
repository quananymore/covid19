//package com.ifi.covid19.controller;
//
//import com.ifi.covid19.DTO.ScheduleDTO;
//import com.ifi.covid19.Input.ScheduleInput;
//import com.ifi.covid19.common.Output;
//import com.ifi.covid19.model.Schedule;
//import com.ifi.covid19.service.AddressService;
//import com.ifi.covid19.service.ScheduleService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.ResourceBundle;
//
//@CrossOrigin(origins = "http://localhost:3000")
//@RestController
//public class ScheduleController {
//    Logger log = LoggerFactory.getLogger(ScheduleController.class);
//
//    @Autowired
//    private ScheduleService scheduleService;
//
//    @Autowired
//    private AddressService addressService;
//
//    @PostMapping("/schedule/add")
//    public Output addContact(@RequestBody ScheduleInput schedule) {
//        ResourceBundle r = ResourceBundle.getBundle("Bundle/config_en");
//        Output out = new Output();
//        scheduleService.save(schedule.toModel());
//        out.setMessage(r.getString("1"));
//        return out;
//    }
//
//
//    @GetMapping("/schedule/{patient_id}")
//    public List<ScheduleDTO> getScheduleByPatient(@PathVariable("patient_id") Long id) {
//        List<Schedule> schedules = scheduleService.findByPatientId(id);
//        List<ScheduleDTO> scheduleDTOS = new ArrayList<>();
//        for (Schedule schedule : schedules) {
//            scheduleDTOS.add(convertDTOHos(schedule));
//        }
//        return scheduleDTOS;
//    }
//
//    public ScheduleDTO convertDTOHos(Schedule schedule) {
//        ScheduleDTO patientDTO =schedule.toDTO();
//        patientDTO.setProvince(addressService.findProvinceById(schedule.getProvinceId()).getName());
//        patientDTO.setWard(addressService.findWardById(schedule.getWardId()).getName());
//        patientDTO.setDistrict(addressService.findDistrictById(schedule.getDistrictId()).getName());
//        return patientDTO;
//    }
//
//
//}
