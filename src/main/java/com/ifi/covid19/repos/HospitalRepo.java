package com.ifi.covid19.repos;

import com.ifi.covid19.model.Hospital;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HospitalRepo extends JpaRepository<Hospital,Long> {

    @Query("SELECT p FROM Hospital p where p.id =:id")
    Hospital findByCode(@Param("id")Long id);

//    Hos findByCode(@Param("code") Long code);
}
