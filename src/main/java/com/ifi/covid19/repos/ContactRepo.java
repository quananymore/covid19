package com.ifi.covid19.repos;

import com.ifi.covid19.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContactRepo extends JpaRepository<Contact,Long> {
    @Query("SELECT p FROM Contact p where p.patientId =:id")
    List<Contact> findByPatientId(Long id);
}
