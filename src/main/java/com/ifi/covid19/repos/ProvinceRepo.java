package com.ifi.covid19.repos;

import com.ifi.covid19.model.Patient;
import com.ifi.covid19.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
public interface ProvinceRepo extends JpaRepository<Province,Long> {
    @Query("SELECT p FROM Province p where p.code =:code")
    Province findByCode(@Param("code") Long code);
}
