package com.ifi.covid19.repos;

import com.ifi.covid19.model.Province;
import com.ifi.covid19.model.Ward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WardRepository extends JpaRepository<Ward,Long> {
    @Query("SELECT p FROM Ward p where p.code =:code")
    Ward findByCode(@Param("code") Long code);

    @Query("SELECT p FROM Ward p where p.district_code =:code")
    List<Ward> findByDistrict(@Param("code")Long id);
}
