package com.ifi.covid19.repos;

import com.ifi.covid19.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
public interface PatientRepository extends JpaRepository<Patient,Long> {

    @Query("SELECT p FROM Patient p where p.isDelete = 0 and p.status =:status")
    List<Patient> findAllByStatus(@Param("status") Long status);

    @Query("SELECT p FROM Patient p where p.isDelete = 0 and p.status =:status and p.name LIKE CONCAT('%',:name,'%')")
    List<Patient> findAllByStatusAndKeyword(@Param("status") Long status, @Param("name") String name);

    @Query("SELECT p FROM Patient p where p.isDelete = 0")
    List<Patient> findAllActive();

    @Query("SELECT p FROM Patient p where p.name LIKE CONCAT('%',:name,'%')")
    List<Patient> findAllByKeyword(@Param("name") String name);
}
