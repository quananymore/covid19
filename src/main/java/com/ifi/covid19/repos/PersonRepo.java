package com.ifi.covid19.repos;

import com.ifi.covid19.model.Person;
import com.ifi.covid19.model.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonRepo extends JpaRepository<Person,Long> {
    @Query("SELECT p FROM Person p where p.id =:id")
    Person findByCode(@Param("id") Long id);
}
