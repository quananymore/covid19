package com.ifi.covid19.repos;

import com.ifi.covid19.model.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DistrictRepo extends JpaRepository<District,Long> {
    @Query("SELECT p FROM District p where p.code =:code")
    District findByCode(@Param("code") Long code);

    @Query("SELECT p FROM District p where p.province_code =:code")
    List<District> findByProvince(@Param("code")Long id);
}
