package com.ifi.covid19.repos;

import com.ifi.covid19.model.Province;
import com.ifi.covid19.model.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
public interface ScheduleRepo extends JpaRepository<Schedule,Long> {
    @Query("SELECT p FROM Schedule p where p.patientId =:id")
    List<Schedule> findByPatientId(Long id);
}
