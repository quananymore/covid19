package com.ifi.covid19.service;

import com.ifi.covid19.model.Patient;

import java.util.List;

public interface PatientService {
    Patient save(Patient patient);
    List<Patient> findAllActive();
    Patient deletePatient(Long id);
    List<Patient> searchWithStatusAndKeyWord(String kw,Long status);
    List<Patient> searchWithKeyWord(String kw);
    List<Patient> searchWithStatus(Long status);
    Patient findById(Long id);

}
