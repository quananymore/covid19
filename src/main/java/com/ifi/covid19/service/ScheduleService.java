package com.ifi.covid19.service;


import com.ifi.covid19.model.Schedule;

import java.util.List;

public interface ScheduleService {
    List<Schedule> findByPatientId(Long id);
    Schedule save (Schedule schedule);
}
