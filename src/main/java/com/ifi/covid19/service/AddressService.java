package com.ifi.covid19.service;

import com.ifi.covid19.model.District;
import com.ifi.covid19.model.Hospital;
import com.ifi.covid19.model.Province;
import com.ifi.covid19.model.Ward;
import org.springframework.stereotype.Service;

import java.util.List;
public interface AddressService {
    void addProvince(List<Province> list);
    void addDistrict(List<District> list);
    void addWard(Ward ward);

    Ward findWardById(Long id);
    District findDistrictById(Long id);
    Province findProvinceById(Long id);
    Hospital findHosById(Long id);

    List<Hospital> findAllHos();

    List<Province> getProvinceAll();

    List<District> getDistrictByProvince(Long id);

    List<Ward> getWardByDistrict(Long id);
}
