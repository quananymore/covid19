package com.ifi.covid19.service;

import com.ifi.covid19.model.Contact;
import com.ifi.covid19.model.Person;
import com.ifi.covid19.model.Schedule;

import java.util.List;

public interface ContactService {
    List<Contact> findByPatientId(Long id);
    Contact save(Contact contact);
    Person findPerson(Long personId);
}
