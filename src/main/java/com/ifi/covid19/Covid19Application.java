package com.ifi.covid19;

import com.ifi.covid19.DTO.PatientDTO;
import com.ifi.covid19.common.Storage;
import com.ifi.covid19.controller.PatientController;
import com.ifi.covid19.model.Patient;
import com.ifi.covid19.service.PatientService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class Covid19Application {
    public static void main(String[] args) {
        SpringApplication.run(Covid19Application.class, args);
//        TimerTask timerTask = new TimerTask() {
//            @Override
//            public void run() {
//                List<PatientDTO> patientDTOs = new PatientController().getAllPatient();
//                PatientController.test();
//                new Storage().exportExcel(patientDTOs);
//                System.out.println("exportExcel");
//            }
//        };
//        Timer timer = new java.util.Timer();
//        timer.schedule(timerTask, 1, 1800000);
    }
}
